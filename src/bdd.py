#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb as mbd
from config.py import *

class bdd:

	# crée l'objet de connection à la bdd et positionne le curseur
	def __init__(self, host, user, passwd, db):
		self.bdd=mbd.connect(host, user, passwd, db)
		self.cursor=self.bdd.cursor()
		
		
	# execute les requetes qui n'attendent pas de retour
	def execute(self, requete):
		try:
			self.cursor.execute(requete)
			self.bdd.commit()
			
		except:
			print 'Error Requête : ', requete
			return -1
		
	# execute requete et retourne liste des resultats
	def query(self, requete):
		try:
			self.cursor.execute(requete)
			result = self.cursor.fetchall()
			return result
			
		except:
			print 'Error Requête : ', requete
			return -1
		
		
		
if __name__ == '__main__':
	mybdd=bdd(host, user, passwd, db)
