# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

IMG = "../img/"

class MainWindow(QtGui.QMainWindow):
  	def __init__(self):
		QtGui.QWidget.__init__(self)

		self.resize(544, 511)
		self.setWindowTitle("Bikers Manager")

		self.centralwidget = QtGui.QWidget()
		self.gridLayout = QtGui.QGridLayout(self.centralwidget)
		self.splitter = QtGui.QSplitter(self.centralwidget)
		self.splitter.setOrientation(QtCore.Qt.Horizontal)

		### Onglets # Potentiellement clarissable
		self.listWidget = QtGui.QListWidget(self.splitter)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.listWidget.sizePolicy().hasHeightForWidth())
		self.listWidget.setSizePolicy(sizePolicy)
		self.listWidget.setMaximumSize(QtCore.QSize(72, 16777215))
		self.listWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
		self.listWidget.setIconSize(QtCore.QSize(72, 72))
		self.listWidget.setMovement(QtGui.QListView.Static)
		self.listWidget.setGridSize(QtCore.QSize(72, 72))
		self.listWidget.setViewMode(QtGui.QListView.IconMode)
		self.listWidget.setUniformItemSizes(True)
		self.listWidget.setSortingEnabled(False)

		### Icones des onglets
		self.icon_name = ["gnome-documents.svg","addressbook.svg","drakstats.svg","gnucash-icon.svg","gnome-settings-theme.svg"]
		for icon in self.icon_name:
			self.listWidget.addItem(ListIconWidget(icon))

		### Affichage pages
		self.stackedWidget = QtGui.QStackedWidget(self.splitter)
		
		self.stackedWidget.addWidget(PActu()) ### Actus
		self.stackedWidget.addWidget(PAdherents()) ### Adhérents
		self.stackedWidget.addWidget(PStock()) ### Stock
		self.stackedWidget.addWidget(PVente()) ### Vente
		self.stackedWidget.addWidget(PAdmin()) ### Admin

		### Affichage global
		self.gridLayout.addWidget(self.splitter, 0, 0, 1, 1)
		self.setCentralWidget(self.centralwidget)

		### Menubar
		self.menubar = QtGui.QMenuBar()
		self.menubar.setGeometry(QtCore.QRect(0, 0, 544, 24))
		self.setMenuBar(self.menubar)

		### Statusbar
		self.statusbar = QtGui.QStatusBar()
		self.setStatusBar(self.statusbar)

		### Signaux, initialisation des valeurs par défaut et setText en tout genre
		self.listWidget.setCurrentRow(-1)
		self.stackedWidget.setCurrentIndex(0)
		QtCore.QObject.connect(self.listWidget, QtCore.SIGNAL(_fromUtf8("currentRowChanged(int)")), self.stackedWidget.setCurrentIndex)
		QtCore.QMetaObject.connectSlotsByName(self)

class ListIconWidget(QtGui.QListWidgetItem):
	def __init__(self,icon_name):
		QtGui.QListWidgetItem.__init__(self)
		self.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter|QtCore.Qt.AlignCenter)
		self.icon = QtGui.QIcon()
		self.icon.addPixmap(QtGui.QPixmap(_fromUtf8(IMG+icon_name)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.setIcon(self.icon)
		self.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsDropEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)

class PActu(QtGui.QWidget):
	def __init__(self):
		QtGui.QWidget.__init__(self)
		self.verticalLayout = QtGui.QVBoxLayout(self)
		self.groupBox_2 = QtGui.QGroupBox(_fromUtf8("Actu et Récap\'"))
		self.verticalLayout.addWidget(self.groupBox_2)

class PAdherents(QtGui.QWidget):
	def __init__(self): ### Ca je laisse crade parce que on le changera alors pour le moment on s'en fout.
		QtGui.QWidget.__init__(self)
		self.gridLayout_2 = QtGui.QGridLayout(self)
		self.groupBox = QtGui.QGroupBox(_fromUtf8("Adhérents"))
		self.gridLayout_3 = QtGui.QGridLayout(self.groupBox)
		self.splitter_3 = QtGui.QSplitter(self.groupBox)
		self.splitter_3.setOrientation(QtCore.Qt.Horizontal)
		self.widget = QtGui.QWidget(self.splitter_3)
		self.verticalLayout_5 = QtGui.QVBoxLayout(self.widget)
		self.verticalLayout_5.setMargin(0)
		self.lineEdit = QtGui.QLineEdit(self.widget)
		self.verticalLayout_5.addWidget(self.lineEdit)
		self.tableWidget = QtGui.QTableWidget(self.widget)
		self.tableWidget.setColumnCount(0)
		self.tableWidget.setRowCount(0)
		self.verticalLayout_5.addWidget(self.tableWidget)
		self.splitter_2 = QtGui.QSplitter(self.splitter_3)
		self.splitter_2.setOrientation(QtCore.Qt.Vertical)
		self.frame = QtGui.QFrame(self.splitter_2)
		self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
		self.gridLayout_5 = QtGui.QGridLayout(self.frame)
		self.frame.setFrameShadow(QtGui.QFrame.Raised)
		self.label_2 = QtGui.QLabel(self.frame)
		self.gridLayout_5.addWidget(self.label_2, 0, 0, 1, 1)
		self.frame_2 = QtGui.QFrame(self.splitter_2)
		self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
		self.gridLayout_4 = QtGui.QGridLayout(self.frame_2)
		self.verticalLayout_3 = QtGui.QVBoxLayout()
		self.label = QtGui.QLabel(self.frame_2)
		self.verticalLayout_3.addWidget(self.label)
		self.pushButton = QtGui.QPushButton(self.frame_2)
		self.verticalLayout_3.addWidget(self.pushButton)
		self.gridLayout_4.addLayout(self.verticalLayout_3, 0, 0, 1, 1)
		self.gridLayout_3.addWidget(self.splitter_3, 0, 0, 1, 1)
		self.gridLayout_2.addWidget(self.groupBox, 0, 0, 1, 1)
		self.label_2.setText("Data base info")
		self.label.setText("Membre en cours")
		self.pushButton.setText("Modifier")

class PStock(QtGui.QWidget):
	def __init__(self):
		QtGui.QWidget.__init__(self)
		self.verticalLayout_2 = QtGui.QVBoxLayout(self)
		self.groupBox_3 = QtGui.QGroupBox("Stock")
		self.verticalLayout_2.addWidget(self.groupBox_3)

class PVente(QtGui.QWidget):
	def __init__(self):
		QtGui.QWidget.__init__(self)
		self.verticalLayout_4 = QtGui.QVBoxLayout(self)
		self.groupBox_4 = QtGui.QGroupBox("Vente")
		self.verticalLayout_4.addWidget(self.groupBox_4)

class PAdmin(QtGui.QWidget):
	def __init__(self):
		QtGui.QWidget.__init__(self)
		self.gridLayout_6 = QtGui.QGridLayout(self)
		self.groupBox_5 = QtGui.QGroupBox("Admin")
		self.gridLayout_6.addWidget(self.groupBox_5, 0, 0, 1, 1)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    mainwindow = MainWindow()
    mainwindow.show()
    sys.exit(app.exec_())

