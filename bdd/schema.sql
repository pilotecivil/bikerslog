-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 19 Mars 2014 à 08:53
-- Version du serveur: 5.5.35
-- Version de PHP: 5.4.4-14+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `bikers_dev`
--
CREATE DATABASE `bikers_dev` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bikers_dev`;

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

CREATE TABLE IF NOT EXISTS `adherent` (
  `id_adherent` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `departement` varchar(10) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `statut` set('adhérent','staff') NOT NULL,
  `date_adhesion` datetime NOT NULL,
  PRIMARY KEY (`id_adherent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE IF NOT EXISTS `compte` (
  `id_transaction` int(11) NOT NULL AUTO_INCREMENT,
  `code_transaction` int(1) NOT NULL,
  `id_reference` int(11) NOT NULL,
  `montant_transaction` float NOT NULL,
  `montant_cumul` float NOT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `piece_neuve`
--

CREATE TABLE IF NOT EXISTS `piece_neuve` (
  `id_piece_neuve` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `prix` float NOT NULL,
  `quantite` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id_piece_neuve`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `piece_occasion`
--

CREATE TABLE IF NOT EXISTS `piece_occasion` (
  `id_piece_occasion` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `prix` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id_piece_occasion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `velo`
--

CREATE TABLE IF NOT EXISTS `velo` (
  `id_velo` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `prix_achat` float NOT NULL,
  `date_achat` datetime NOT NULL,
  `prix_vente` float DEFAULT NULL,
  `date_vente` datetime DEFAULT NULL,
  PRIMARY KEY (`id_velo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
